import json
import os
import requests
from nose.tools import *
import sys
import unittest

URL = 'https://apolo-profile-dev.ausvdc02.pcf.dell.com'

class TestMessagesHistoric(unittest.TestCase):

    def test_get_messages_historic(self):
        response = requests.get(f'{URL}/messages/historic', verify=False)
        assert_equal(response.status_code, 200)

        json_data = json.loads(response.content)

        print(json_data)

        assert_in('recommendations', json_data)
        recom = json_data['recommendations']
        assert_true(recom)

        first = recom[0]
        assert_equal(type(first), dict)

        assert_in('id', first)
        assert_equal(type(first['id']), str)

        assert_in('initialMessage', first)
        assert_equal(type(first['initialMessage']), str)

        assert_in('recommendation', first)
        assert_equal(type(first['recommendation']), str)

        assert_in('validation', first)
        assert_equal(type(first['validation']), str)

        assert_in('userSuggestion', first)
        assert_equal(type(first['userSuggestion']), str)
        

        assert_in('currentPage', json_data)
        assert_equal(type(json_data['currentPage']), int)

        assert_in('numPages', json_data)
        assert_equal(type(json_data['numPages']), int)

        assert_in('itemsPerPage', json_data)
        assert_equal(type(json_data['itemsPerPage']), int)

        assert_in('totalItens', json_data)
        assert_equal(type(json_data['totalItens']), int)

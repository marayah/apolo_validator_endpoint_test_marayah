import requests
import json
from nose.tools import *

URL = 'https://apolo-profile-dev.ausvdc02.pcf.dell.com'

def get_user_id(self):
    response = requests.get(f'{URL}/users', verify=False)
    assert_equal(response.status_code, 200)

    json_data = json.loads(response.content)

    assert_in('users', json_data)
    users = json_data['users']
    assert_true(users)

    user = users[0]
    id = user['id']

    return id

def set_new_user(self):
    send = {
        'email': 'test_post@dellead.com',
        'name': 'Test Post 3',
        'role': 'Visitor',
        'nick': 'TEST_P'
    }

    return send

def get_users_num_pages(self):
    response = requests.get(f'{URL}/users', verify=False)
    assert_equal(response.status_code, 200)

    json_data = json.loads(response.content)

    assert_in('numPages', json_data)
    numPages = json_data['numPages']

    return numPages

def get_user_id_nick(self, nick, page):
    response = requests.get(f'{URL}/users?page={page}', verify=False)
    assert_equal(response.status_code, 200)

    json_data = json.loads(response.content)

    assert_in('users', json_data)
    users = json_data['users']
    assert_true(users)

    id = ''
    for user in users:
        if user['nick'] == nick:
            id = user['id']
            break
    
    return id

def set_new_role(self):
    send = {
        'role': 'Validator'
    }

    return send
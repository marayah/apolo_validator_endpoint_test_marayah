import json
import os
import requests
from nose.tools import *
import sys
import unittest

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
path_dir = os.path.join(ROOT_DIR, "json_scenarios")
sys.path.append(path_dir)

from json_utils import *

URL = 'https://apolo-profile-dev.ausvdc02.pcf.dell.com'

class TestUsers(unittest.TestCase):

    def test_get_users_1(self):
        response = requests.get(f'{URL}/users', verify=False)
        assert_equal(response.status_code, 200)

        json_data = json.loads(response.content)

        assert_in('users', json_data)
        users = json_data['users']
        assert_true(users)

        first = users[0]
        assert_equal(type(first), dict)
        
        assert_in('id', first)
        assert_equal(type(first['id']), str)

        assert_in('email', first)
        assert_equal(type(first['email']), str)

        assert_in('name', first)
        assert_equal(type(first['name']), str)

        assert_in('role', first)
        assert_equal(type(first['role']), str)

        assert_in('lastAcess', first)
        assert_equal(type(first['lastAcess']), str)


        assert_in('currentPage', json_data)
        assert_equal(type(json_data['currentPage']), int)

        assert_in('numPages', json_data)
        assert_equal(type(json_data['numPages']), int)

        assert_in('itemsPerPage', json_data)
        assert_equal(type(json_data['itemsPerPage']), int)

        assert_in('totalItens', json_data)
        assert_equal(type(json_data['totalItens']), int)
    
    def test_get_users_id(self):
        id = get_user_id(self)

        response = requests.get(f'{URL}/users/{id}', verify=False)
        assert_equal(response.status_code, 200)

        json_data = json.loads(response.content)

        assert_in('id', json_data)
        assert_equal(type(json_data['id']), str)

        assert_in('email', json_data)
        assert_equal(type(json_data['email']), str)

        assert_in('name', json_data)
        assert_equal(type(json_data['name']), str)

        assert_in('role', json_data)
        assert_equal(type(json_data['role']), str)

        assert_in('lastAcess', json_data)
        assert_equal(type(json_data['lastAcess']), str)
    
    # def test_post_users(self):
    #     user_data = set_new_user(self)

    #     response = requests.post(f'{URL}/users/', verify=False, json=user_data)
    #     assert_equal(response.status_code, 201)

    #     json_data = json.loads(response.content)
    #     print(json_data)

    def test_delete_users(self):
        user_data = set_new_user(self)

        response_post = requests.post(f'{URL}/users/', verify=False, json=user_data)
        assert_equal(response_post.status_code, 201)

        json_data_post = json.loads(response_post.content)
        print(json_data_post)

        numPages = get_users_num_pages(self)
        page = 1
        for page in range(numPages+1):
            id_delete = get_user_id_nick(self, 'TEST_P', page)
            if id_delete != '':
                break
            page += 1
        
        response_delete = requests.delete(f'{URL}/users/{id_delete}', verify=False)
        assert_equal(response_delete.status_code, 200)

        json_data_delete = json.loads(response_delete.content)
        print(json_data_delete)

    def test_patch_users(self):
        user_data = set_new_user(self)

        response_post = requests.post(f'{URL}/users/', verify=False, json=user_data)
        assert_equal(response_post.status_code, 201)

        json_data_post = json.loads(response_post.content)
        print(json_data_post)

        numPages = get_users_num_pages(self)
        page = 1
        for page in range(numPages+1):
            id = get_user_id_nick(self, 'TEST_P', page)
            if id != '':
                break
            page += 1

        user_role = set_new_role(self)

        response = requests.patch(f'{URL}/users/{id}', verify=False, json=user_role)
        assert_equal(response.status_code, 200)

        json_data = json.loads(response.content)
        print(json_data)
